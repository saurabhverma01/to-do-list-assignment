var Tasks = /** @class */ (function () {
    function Tasks(tasks) {
        this.tasks = tasks;
    }
    Tasks.prototype.add = function (task, status) {
        var temp = {
            task: task,
            status: status
        };
        this.tasks.push(temp);
        return this.tasks.length - 1;
    };
    Tasks.prototype.display = function () {
        return this.tasks;
    };
    return Tasks;
}());
var tasks = new Tasks([]);
function addTasks(task) {
    console.log(tasks.tasks);
    return tasks.add(task, 0);
}
function display() {
    return tasks.display();
}
