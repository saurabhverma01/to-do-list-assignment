

interface taskList{
    task:string,
    status:number  // 0-incomplete 1-completed 2-deleted
}
class Tasks{
    public tasks:taskList[];

    constructor(tasks:taskList[]){
            this.tasks=tasks;
    }

    add(task:string,status:number){
       let temp:taskList={
           task:task,
           status:status
       };
       this.tasks.push(temp);
      return  this.tasks.length-1;
    }

    display(){
        return this.tasks;
    }
}

var tasks=new Tasks([]);

function addTasks(task:string){
    console.log(tasks.tasks);
   return tasks.add(task,0);

}

function display(){
    return tasks.display();
}