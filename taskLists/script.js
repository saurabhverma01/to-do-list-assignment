
window.onload = function() {
    var input = document.getElementById('myInput');
    var btn = document.getElementById('btn');
    var taskList=document.getElementById('myUL');
    //creating a close button near in list items
    var myNodelist = document.getElementsByTagName('li');

    var i;
    for (i = 0; i < myNodelist.length; i++) {
        var span = document.createElement("SPAN");
        var txt = document.createTextNode("\u00D7");
        span.className = "close";
        span.appendChild(txt);
        myNodelist[i].appendChild(span);
    }
//functionality of Add button
    btn.onclick = function () {
        var value = input.value;
        if(value==""){
            alert("you must write something");
        }else{
        var listItem=display();
        var flag=0;
        for(var i=0;i<listItem.length;i++){
            if(value==listItem[i].task && listItem[i].status!=2){
                flag=1;break;
            }
        }
        if(flag==1){
            alert("this task is already added");
        }else{
            var index=addTasks(value);
            input.value="";
            var listItem=display()[index];
            var li = document.createElement("li");
            var t = document.createTextNode(listItem.task);
            var checkbox = document.createElement('input');
            checkbox.type = "checkbox";
            checkbox.name = "name";
            checkbox.value = "value";
            checkbox.className = "isActive";
            var eventCheck=0;
            checkbox.onclick=function () { //mark as complete any task
                eventCheck=1;
                if(listItem.status==0){
                listItem.status=1;
                li.setAttribute("class","isComplete");}
                else{
                    listItem.status=0;
                    li.setAttribute("class","#");
                }
            }

            li.appendChild(checkbox);
            li.appendChild(t);
            taskList.appendChild(li);
            var span = document.createElement("SPAN");
            var txt = document.createTextNode("\u00D7");
            span.className = "close";
            span.appendChild(txt);
            li.appendChild(span);
            span.onclick=function() {   // delete button functionality
                var div = this.parentElement;
                var str=div.innerText;
                str=str.substr(0,str.length-1);
                console.log(str);
                var list=display();
                for(var i=0;i<list.length;i++){
                    if(list[i].task==str){
                        list[i].status=2;
                        break;
                    }
                }
                console.log(list[i]);
                div.style.display = "none";
            }
            li.onclick=function () {
                if(eventCheck==0){
                    eventCheck=2;
                var div=document.createElement('div');
                var edit=document.createElement('input');
                edit.setAttribute("class","edit");
                var str=li.innerText;
                str=str.substr(0,str.length-1);
                edit.setAttribute("value",str);
                var span = document.createElement("SPAN");
                var txt = document.createTextNode("update");
                span.setAttribute("class","updateBtn");
                span.appendChild(txt);
                div.appendChild(edit);
                div.appendChild(span);
                li.replaceChild(div,li.childNodes[1]);
                span.onclick=function () {
                    var list=display();
                    for(var i=0;i<list.length;i++){
                        if(list[i].task==str){
                            list[i].task=edit.value;
                            break;
                        }
                    }
                    li.replaceChild(document.createTextNode(list[i].task),li.childNodes[1]);
                    eventCheck--;
                    console.log(eventCheck);
                }
            }
            else if(eventCheck==1){
                    eventCheck=0;
                }
            }
        }
        }
    };
//functionality of the delete button
    var closeBtn=document.getElementsByClassName('close');
    for (i = 0; i < closeBtn.length; i++) {
        closeBtn[i].onclick = function() {
            var div = this.parentElement;
            var str=div.innerText;
            str=str.substr(0,str.length-1);
            console.log(str);
            var list=display();
            for(var i=0;i<list.length;i++){
                if(list[i].task==str){
                    list[i].status=2;
                    break;
                }
            }
            console.log(list[i]);
            div.style.display = "none";
        }
    }
    
}